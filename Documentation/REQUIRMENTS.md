# Requirements


## Functional Requirements

1. add item to check list 
2. delete item from check list
3. show and modify items in the check list


## non Functional Requirements

1. Frindly user interface
2. Secure transaction between server and clients 
3. Fast respond
4. Cross browser consistency
5. Full log
