<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'traveler check list') }}</title>
    <link rel="stylesheet" href="{{ asset('css/google-fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/todo.css') }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/plugins/sweetalert/sweetalert.css") }}" type="text/css"/>
</head>
<body>
<main id="todolist">
  <h1>چک لیست مسافرتی<span>هیچ وقت وسایلتو فراموش نکن</span></h1>

  <template v-if="todo.length">
    <transition-group name="todolist" tag="ul">
      <li v-for="item in todoByStatus"  v-bind:class=" (item.done)?'':'done'" v-bind:key="item.id">
      <span class="label">@{{item.label}}</span>
        <div class="actions">
            <button class="btn-picto" type="button" v-on:click="markAsDoneOrUndone(item)" v-bind:aria-label="item.done ? 'Done' : 'Undone'" v-bind:title="item.done ? 'Done' : 'Undone'">
                 <i aria-hidden="true" class="material-icons">@{{ item.done ? 'check_box_outline_blank' : 'check_box' }}</i>
           </button>
          <button class="btn-picto" type="button" v-on:click="deleteItemFromList(item)" aria-label="Delete" title="Delete">
            <i aria-hidden="true" class="material-icons">delete</i>
          </button>
        </div>
      </li>
    </transition-group>
    <togglebutton 
        label="انجام شده ها رو بیار پایین;) "
        name="todosort"
        v-on:clicked="clickontoogle" />
  </template>
  <p v-else class="emptylist"> لیست شما خالی است</p>

  <form name="newform" v-on:submit.prevent="addItem">
    <label for="newitem">اضافه کردن TODO</label>
    <input type="text" name="newitem" id="newitem" v-model="newitem">
    @csrf
    <button type="submit">ثبت</button>
  </form>
</main>
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/vue.min.js') }}"></script>
<script src="{{ asset("admin/assets/plugins/sweetalert/sweetalert.min.js") }}"></script>
<script>
    Vue.component('togglebutton', {
  props: ['label', 'name'],
  template: `<div class="togglebutton-wrapper" v-bind:class="isactive ? 'togglebutton-checked' : ''">
      <label v-bind:for="name">
        <span class="tooglebutton-box"></span>
        <span class="togglebutton-label">@{{ label }}</span>
      </label>
      <input v-bind:id="name" type="checkbox" v-bind:name="name" v-model="isactive" v-on:change="onToogle">
  </div>`,
  model: {
    prop: 'checked',
    event: 'change'
  },
  data: function() {
    return {
      isactive:false
    }
  },
  methods: {
    onToogle: function() {
       this.$emit('clicked', this.isactive)
    }
  }
});

var todolist = new Vue({
  el: '#todolist',
  data: {
    newitem:'',
    sortByStatus:false,
    todo: @php echo $todos @endphp
  },
  methods: {
    addItem: function() {
      var id;
      $.post("/todo",{
        label  : this.newitem,
        _token : $('input[name=_token]').val(),
        done   : false,
        category_id : {{ $category->id }}
      },function(data, status){
        todolist.todo.push({id: data.id , label: data.label, done: true});
        id = data.id
      });
      this.newitem = '';
    },
    markAsDoneOrUndone: function(item) {
      item.done = !item.done;
    },
    deleteItemFromList: function(item) {
        swal({
            title: "آیا مطمئن هستید؟",
            text: "این آیتم پاک شود؟",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "بلی",
            cancelButtonText: "خیر",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                let index = todolist.todo.indexOf(item)
                $.post('/todo/'+item.id,{
                    _token : $('input[name=_token]').val(),
                    _method: 'delete'
                },function(data, status){});
                todolist.todo.splice(index, 1);
            }
        });
    },
    clickontoogle: function(active) {
      this.sortByStatus = active;
    },
  },
  computed: {
    todoByStatus: function() {

      if(!this.sortByStatus) {
        return this.todo;
      }

      var sortedArray = []
      var doneArray = this.todo.filter(function(item) { return !item.done; });
      var notDoneArray = this.todo.filter(function(item) { return item.done; });
      
      sortedArray = [...notDoneArray, ...doneArray];
      return sortedArray;
    }
  }
});
</script>
</body>
</html>