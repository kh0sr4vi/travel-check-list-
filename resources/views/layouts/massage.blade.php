@if(Session::has('message'))
    <script type="text/javascript">
        swal("عملیات انجام شد", '{{ Session::get('message') }}', "success");
    </script>
@endif

@if(count($errors) > 0)
    @php $text = '' @endphp
    @foreach($errors->all() as $error)
        @php $text .= $error . '\n' @endphp
    @endforeach

    <script type="text/javascript">
        swal({
            title: "خطا",
            text: '{{ $text }}',
            type: "warning",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "{{trans('labels.close')}}",
            closeOnConfirm: false
        });
    </script>
@endif

<script type="text/javascript">
    addressValue = '';
    $('.remove-item').on('click', function (e) {
        e.preventDefault();
        window.addressValue = $(this).attr("href");
        swal({
            title: "آیا مطمئن هستید؟",
            text: "این آیتم پاک شود؟",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "بلی",
            cancelButtonText: "خیر",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                window.location.href = window.addressValue;
            }
        });
    });
    $('.add-to-selected').on('click', function (e) {
        e.preventDefault();
        window.addressValue = $(this).attr("href");
        swal({
            title: "آیا مطمئن هستید؟",
            text: "این آیتم به منتخب ها اضافه شود؟",
            type: "info",
            showCancelButton: true,
            //confirmButtonColor: "#61534e",
            confirmButtonText: "بلی",
            cancelButtonText: "خیر",
            //closeOnConfirm: false,
            //closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                window.location.href = window.addressValue;
            }
        });
    });
    $('.add-to-slider').on('click', function (e) {
        e.preventDefault();
        window.addressValue = $(this).attr("href");
        console.log(window.addressValue);
        $('#sliderVideId').attr('value',window.addressValue);
        /*swal({
            content: "input",
            title: "آیا مطمئن هستید؟",
            text: "این آیتم به منتخب ها اضافه شود؟",
            type: "info",
            showCancelButton: true,
            //confirmButtonColor: "#61534e",
            confirmButtonText: "بلی",
            cancelButtonText: "خیر",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                window.location.href = window.addressValue;
            }
        });*/
    });
</script>