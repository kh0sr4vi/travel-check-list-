<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset("admin/assets/plugins/sweetalert/sweetalert.css") }}" type="text/css"/>
</head>
<body>
    
<!-- Mixins-->
<!-- Pen Title-->
<div class="pen-title">
  </div>
  <div class="container">
    <div class="card"></div>
    <div class="card">
      <h1 class="title">ورود</h1>
      <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="input-container">
          <input type="text" name="username" id="#{label}" value="{{ old('username') }}" required="required"/>
          <label for="#{label}">نام کاربری</label>
          <div class="bar"></div>
        </div>
        <div class="input-container">
          <input type="password" name="password" id="#{label}" required="required"/>
          <label for="#{label}">رمزعبور</label>
          <div class="bar"></div>
        </div>
        <div class="button-container">
          <button><span>ورود</span></button>
        </div>
      </form>
    </div>
    <div class="card alt">
      <div class="toggle"></div>
      <h1 class="title">ثبت نام
        <div class="close"></div>
      </h1>
      <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="input-container">
                <input type="text" name="name" id="#{label}" value="{{ old('name') }}" required="required"/>
                <label for="#{label}">نام </label>
                <div class="bar"></div>
              </div>
        <div class="input-container">
          <input type="text" name="username" id="#{label}" value="{{ old('username') }}" required="required"/>
          <label for="#{label}">نام کاربری</label>
          <div class="bar"></div>
        </div>
        <div class="input-container">
          <input type="password" name="password" id="#{label}" required="required"/>
          <label for="#{label}">رمزعبور</label>
          <div class="bar"></div>
        </div>
        <div class="input-container">
          <input type="password" name="password_confirmation" id="#{label}" required="required"/>
          <label for="#{label}">تکرار رمز عبور</label>
          <div class="bar"></div>
        </div>
        <div class="button-container">
          <button><span>ثبت نام</span></button>
        </div>
      </form>
    </div>
  </div>
  <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset("admin/assets/plugins/sweetalert/sweetalert.min.js") }}"></script>
  @include('layouts/massage')
  <script>
  $('.toggle').on('click', function() {
  $('.container').stop().addClass('active');
});

$('.close').on('click', function() {
  $('.container').stop().removeClass('active');
});
</script>
</body>
</html>