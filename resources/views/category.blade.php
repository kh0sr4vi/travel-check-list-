<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'traveler check list') }}</title>
    <link rel="stylesheet" href="{{ asset('css/google-fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/todo.css') }}">
</head>
<body>
<main id="todolist">
  <h1>چک لیست مسافرتی<span>هیچ وقت وسایلتو فراموش نکن</span></h1>

  <template v-if="todo.length">
    <transition-group name="todolist" tag="ul">
      <li v-for="item in todoByStatus" v-bind:key="item.id">
      <span v-on:click="goToDo(item)" class="label">@{{item.name}}</span>
        <div class="actions">
          <button class="btn-picto" type="button" v-on:click="deleteItemFromList(item)" aria-label="Delete" title="Delete">
            <i aria-hidden="true" class="material-icons">delete</i>
          </button>
        </div>
      </li>
    </transition-group>
  </template>
  <p v-else class="emptylist">دسته بندی شما خالی است</p>

  <form name="newform" v-on:submit.prevent="addItem">
    <label for="newitem">اضافه کردن دسته بندی</label>
    <input type="text" name="newitem" id="newitem" v-model="newitem">
    @csrf
    <button type="submit">ثبت</button>
  </form>
</main>
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/vue.min.js') }}"></script>
<script>
    Vue.component('togglebutton', {
  props: ['label', 'name'],
  template: `<div class="togglebutton-wrapper" v-bind:class="isactive ? 'togglebutton-checked' : ''">
      <label v-bind:for="name">
        <span class="tooglebutton-box"></span>
        <span class="togglebutton-label">@{{ label }}</span>
      </label>
      <input v-bind:id="name" type="checkbox" v-bind:name="name" v-model="isactive" v-on:change="onToogle">
  </div>`,
  model: {
    prop: 'checked',
    event: 'change'
  },
  data: function() {
    return {
      isactive:false
    }
  },
  methods: {
    onToogle: function() {
       this.$emit('clicked', this.isactive)
    }
  }
});

var todolist = new Vue({
  el: '#todolist',
  data: {
    newitem:'',
    sortByStatus:false,
    todo: @php echo $categories @endphp
  },
  methods: {
    addItem: function() {
      var id;
      $.post("/category",{
        name   : this.newitem,
        _token : $('input[name=_token]').val()
      },function(data, status){
        todolist.todo.push({id: data.id , name: data.name, done: false});
        id = data.id
      });
      this.newitem = '';
    },
    markAsDoneOrUndone: function(item) {
      item.done = !item.done;
    },
    deleteItemFromList: function(item) {
      let index = this.todo.indexOf(item)
      $.post('/category/'+item.id,{
        _token : $('input[name=_token]').val(),
        _method: 'delete'
      },function(data, status){
      });
      this.todo.splice(index, 1);
    },
    clickontoogle: function(active) {
      this.sortByStatus = active;
    },
    goToDo: function(item){
      window.location.href = '/category/'+item.id;
    }
  },
  computed: {
    todoByStatus: function() {

      if(!this.sortByStatus) {
        return this.todo;
      }

      var sortedArray = []
      var doneArray = this.todo.filter(function(item) { return item.done; });
      var notDoneArray = this.todo.filter(function(item) { return !item.done; });
      
      sortedArray = [...notDoneArray, ...doneArray];
      return sortedArray;
    }
  }
});
</script>
</body>
</html>