<?php
return [
	'plan-inserted' => 'plan-inserted',
	'no-data' => 'اطلاعاتی برای نمایش وجود ندارد.',
	'Please Enter From' => 'لطفا حداقل تعداد را وارد کنید',
	'Please Enter To' => 'لطفا حداکثر تعداد را وارد کنید',
	'Please Enter Price' => 'لطفا هزینه را وارد کنید',

	'Customer Inserted Successfully' => 'Customer Inserted Successfully',
	'Customer Updated Successfully' => 'Customer Updated Successfully',
	'Customer Deleted Successfully' => 'Customer Deleted Successfully',

	'Card Inserted Successfully' => 'Card Inserted Successfully',
	'Card Updated Successfully' => 'Card Updated Successfully',
	'Card Deleted Successfully' => 'Card Deleted Successfully',

	'Shop Updated Successfully' => 'Shop Updated Successfully',
	'Shop Deleted Successfully' => 'Shop Deleted Successfully',
	'Shop Inserted Successfully' => 'فروشگاه با موفقیت ثبت شد',
	'Location-inserted' => 'منطقه با موفقیت ثبت شد',
	'Location-updated' => 'منطقه با موفقیت ویرایش شد',

	'user-inserted' => 'کاربر با موفقیت ثبت شد',

	'Are You Sure Delete This Item...?' => 'آیا از حذف این مورد مطمین هستید؟',
	'Plan Not Found' => 'پلن یافت نشد',

	/*schemes messages*/
	'schemes' => [
		'inserted' => 'طرح جدید با موفقیت ثبت گردید',
		'updated' => 'طرح  با موفقیت ویرایش گردید',
		'deleted' => 'طرح  با موفقیت حذف گردید',
	],
	/*score_schemes messages*/
	'score_schemes' => [
		'inserted' => 'طرح امتیازی جدید با موفقیت ثبت گردید',
		'updated' => 'طرح اعتباری  با موفقیت ویرایش گردید',
		'deleted' => 'طرح امتیازی با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این طرح امتیازی اطمینان دارید؟',
	],
	/*credit_schemes messages*/
	'credit_schemes' => [
		'inserted' => 'طرح اعتباری جدید با موفقیت ثبت گردید',
		'updated' => 'طرح اعتباری  با موفقیت ویرایش گردید',
		'deleted' => 'طرح اعتباری با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این طرح اعتباری اطمینان دارید؟',

	],
	/*credit_schemes messages*/
	'credit_schemes2' => [
		'inserted' => 'طرح اعتباری۲ جدید با موفقیت ثبت گردید',
		'updated' => 'طرح اعتباری۲  با موفقیت ویرایش گردید',
		'deleted' => 'طرح اعتباری۲ با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این طرح اعتباری۲ اطمینان دارید؟',

	],
	/*num_discount_schemes messages*/
	'num_discount_schemes' => [
		'inserted' => 'طرح تخفیف-تعداد جدید با موفقیت ثبت گردید',
		'updated' => 'طرح تخفیف-تعداد  با موفقیت ویرایش گردید',
		'deleted' => 'طرح تخفیف-تعداد با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این طرح تخفیف-تعداد اطمینان دارید؟',

	],
	/*coupon_code_schemes messages*/
	'coupon_code_schemes' => [
		'inserted' => 'طرح کوپن کد جدید با موفقیت ثبت گردید',
		'updated' => 'طرح کوپن کد  با موفقیت ویرایش گردید',
		'deleted' => 'طرح کوپن کد با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این طرح کوپن کد اطمینان دارید؟',

	],
	/*combinatorial_schemes messages*/
	'combinatorial_schemes' => [
		'inserted' => 'طرح ترکیبی جدید با موفقیت ثبت گردید',
		'updated' => 'طرح ترکیبی  با موفقیت ویرایش گردید',
		'deleted' => 'طرح ترکیبی با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این طرح ترکیبی اطمینان دارید؟',
		'credit_deleted_question' => 'از حذف این طرح اعتباری اطمینان دارید؟',
		'score_deleted_question' => 'از حذف این طرح امتیازی اطمینان دارید؟',

	],
	/*manifold_schemes messages*/
	'manifold_schemes' => [
		'inserted' => 'طرح امتیاز چند برابر جدید با موفقیت ثبت گردید',
		'updated' => 'طرح امتیاز چند برابر  با موفقیت ویرایش گردید',
		'deleted' => 'طرح امتیاز چند برابر با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این طرح امتیاز چند برابر اطمینان دارید؟',
		'day_hint' => 'لطفا عدد وارد کنید.',
	],
	/*manifold_schemes messages*/
	'discount_schemes' => [
		'inserted' => 'طرح تخفیفی جدید با موفقیت ثبت گردید',
		'updated' => 'طرح تخفیفی  با موفقیت ویرایش گردید',
		'deleted' => 'طرح تخفیفی با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این طرح تخفیفی اطمینان دارید؟',
	],
	'groupCustomers' => [
		'alertDelete' => 'آیا از حذف گروه مطمئن هستید؟',
		'delete' => 'گروه با موفقیت حذف شد.',
		'no-delete-user' => 'به دلیل وجود کاربر برای گروه ، قادر به حذف آن نیستید.',
		'no-delete-scheme' => 'به دلیل وجود طرح برای گروه ، قادر به حذف آن نیستید.',
	],
	'ruleGroup' => [
		'delete' => 'قانون گروه با موفقیت حذف شد.',
		'alertDelete' => 'آیا از حذف قانون مطمئن هستید؟',
	],
	'shop' => [
		'delete' => 'فروشگاه با موفقیت حذف شد.',
		'alertDelete' => 'آیا از حذف فروشگاه مطمئن هستید؟',
		'Shop-Created' => 'فروشگاه با موفقیت ثبت شد.',
	],
	/*combinatorial_schemes messages*/
	'phase_discount_schemes' => [
		'inserted' => 'طرح تخفیف مرحله ای با موفقیت ثبت گردید',
		'updated' => 'طرح تخفیف مرحله ای  با موفقیت ویرایش گردید',
		'deleted' => 'طرح تخفیف مرحله ای با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این طرح تخفیف مرحله ای اطمینان دارید؟',
		'phase_deleted_question' => 'از حذف این مرحله اطمینان دارید؟',
		'phase_deleted' => 'مرحله با موفقیت حذف گردید',

	],
	/*invite_friends_gifts messages*/
	'invite_friends_gifts' => [
		'inserted' => 'هدیه معرفی به دوستان جدید با موفقیت ثبت گردید',
		'updated' => 'هدیه معرفی به دوستان با موفقیت ویرایش گردید',
		'deleted' => 'هدیه معرفی به دوستان با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این هدیه اطمینان دارید؟',
	],
	/*gifts messages*/
	'gifts' => [
		'inserted' => 'هدیه  جدید با موفقیت ثبت گردید',
		'updated' => 'هدیه  با موفقیت ویرایش گردید',
		'deleted' => 'هدیه  با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این هدیه اطمینان دارید؟',
	],
	/*birth_day_gifts messages*/
	'birth_day_gifts' => [
		'inserted' => 'هدیه روز تولد جدید با موفقیت ثبت گردید',
		'updated' => 'هدیه روز تولد با موفقیت ویرایش گردید',
		'deleted' => 'هدیه روز تولد با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این هدیه  ی روز تولد اطمینان دارید؟',
	],
	/*cards messages*/
	'cards' => [
		'inserted' => 'کارت جدید با موفقیت ثبت گردید',
		'inserted_user' => 'کارت و کاربر جدید با موفقیت ثبت گردید',
		'updated' => 'کارت با موفقیت ویرایش گردید',
		'deleted' => 'کارت با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این کارت اطمینان دارید؟',
	],

	'user' => [
		'presented' => 'حضور مشتری در این فروشگاه با موفقیت ثبت گردید.',
		'deletedCard' => 'شماره کارت کاربر حذف گردید.',
		'saveCard' => 'شماره کارت کاربر با موفقیت ثبت گردید.',
		'create' => 'کاربر با موفقیت ثبت شد.',
		'overflow_finalCost' => 'مبلغ فاکتور و تخفیف به درستی وارد نشده‌اند.',
		'addToShop' => 'کاربر با موفقیت اضافه شد.',
		'error_addToShop' => 'این کاربر قبلا عضو فروشگاه شده است.',
		'giftCreate' => 'هدیه برای کاربر با موفقیت ثبت شد.',
		'giftError' => 'این هدیه برای این کاربر استفاده شده است.',
		'emptyGroup' => 'شما هیچ گروهی انتخاب نکرده‌اید.',
		'credit_account' => [
			'inserted' => 'پرداخت با موفقیت صورت گرفت.',
		],
	],

	'user-updated' => 'اطلاعات کاربر با موفقیت ویرایش شد.',

	'bill' => [
		'inserted' => 'صورت حساب با موفقیت ثبت گردید',
		'inserted_user' => 'صورت حساب با موفقیت ثبت گردید',
		'updated' => 'صورت حساب با موفقیت ویرایش گردید',
		'deleted' => 'صورت حساب با موفقیت حذف گردید',
		'deleted_question' => 'از حذف این صورت حساب اطمینان دارید؟',
		'validPay' => 'مبلغ قابل پرداخت به درستی وارد نشده است.',
		'invalidInput' => 'در وارد کردن فیلد ها اشتباهی رخ داده است.',
	],

	'shop_plans' => [
		'deleted_question' => 'از حذف این پلن اطمینان دارید؟',
		'updated' => 'پلن با موفقیت ویرایش گردید',
		'deleted' => 'پلن با موفقیت حذف گردید',
		'inserted' => 'پلن با موفقیت ثبت گردید',

	],
	'plan_card' => [
		'deleted_question' => 'از حذف این کارت اطمینان دارید؟',
		'updated' => 'کارت با موفقیت ویرایش گردید',
		'deleted' => 'کارت با موفقیت حذف گردید',
		'inserted' => 'کارت با موفقیت ثبت گردید',
	],

];
