#  ( travel  check list )

 a project where the user can creat his travel  checklist
###  Services offered by the program

*  the user add to your list
*  The user makes editing your list
*   The user can delete from the list of defined
*  The user can check the status of the tick

##  Key features of the software
*   Avoid wasting time 
*   Easy management and increase customer satisfaction
*  Save money




## The analysis and design project 
* با استفاده از یک سناریو به بررسی کامل خدمات ارائه شده از دید مشتری میپردازیم. [سناریو ها](Documentation/SCENARIO.md)

* [نیازمندیها](Documentation/REQUIRMENTS.md)

##  The development phase of the project

1. Analysis and design of the database 

2.  mplementation of the user interface (UI)

3. The implementation of the program code (BACK END)

##  The developers

| Name and surname |   ID     |
|:----------------:|:--------:|
|    bita khosravi   | @kh0sr4vi |
|     roya abdollahzadeh| @royaabdollahzadeh |        